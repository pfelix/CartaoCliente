﻿namespace CartaoCliente
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelCodCliente = new System.Windows.Forms.Label();
            this.TextBoxCodCliente = new System.Windows.Forms.TextBox();
            this.LabelNome = new System.Windows.Forms.Label();
            this.TextBoxNome = new System.Windows.Forms.TextBox();
            this.GroupBoxResgistoCompras = new System.Windows.Forms.GroupBox();
            this.ButtonRegistarCompra = new System.Windows.Forms.Button();
            this.NumericUpDownValorCompra = new System.Windows.Forms.NumericUpDown();
            this.LabelValorCompra = new System.Windows.Forms.Label();
            this.TextBoxDescricao = new System.Windows.Forms.TextBox();
            this.LabelDescricao = new System.Windows.Forms.Label();
            this.GroupBoxMudarNome = new System.Windows.Forms.GroupBox();
            this.ButtonMudarNome = new System.Windows.Forms.Button();
            this.TextBoxMudarNome = new System.Windows.Forms.TextBox();
            this.LabelMudarNome = new System.Windows.Forms.Label();
            this.GroupBoxRegistarCliente = new System.Windows.Forms.GroupBox();
            this.ButtonRegistarCliente = new System.Windows.Forms.Button();
            this.GroupBoxMudarTaxaPontos = new System.Windows.Forms.GroupBox();
            this.ButtonMudarTaxaPontos = new System.Windows.Forms.Button();
            this.LabelTaxaAtual = new System.Windows.Forms.Label();
            this.LabelNovaTaxa = new System.Windows.Forms.Label();
            this.NumericUpDownNovaTaxa = new System.Windows.Forms.NumericUpDown();
            this.GroupBoxDescontarPontos = new System.Windows.Forms.GroupBox();
            this.ButtonDescontarPontos = new System.Windows.Forms.Button();
            this.NumericUpDownPontosDescontar = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.LabelPontos = new System.Windows.Forms.Label();
            this.GroupBoxTransferirPontos = new System.Windows.Forms.GroupBox();
            this.ButtonTranferir = new System.Windows.Forms.Button();
            this.NumericUpDownPontosTransferir = new System.Windows.Forms.NumericUpDown();
            this.LabelPontosTransferir = new System.Windows.Forms.Label();
            this.TextBoxCodCienteTrsf = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.LabelHistorico = new System.Windows.Forms.Label();
            this.GroupBoxResgistoCompras.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownValorCompra)).BeginInit();
            this.GroupBoxMudarNome.SuspendLayout();
            this.GroupBoxRegistarCliente.SuspendLayout();
            this.GroupBoxMudarTaxaPontos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownNovaTaxa)).BeginInit();
            this.GroupBoxDescontarPontos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownPontosDescontar)).BeginInit();
            this.GroupBoxTransferirPontos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownPontosTransferir)).BeginInit();
            this.SuspendLayout();
            // 
            // LabelCodCliente
            // 
            this.LabelCodCliente.AutoSize = true;
            this.LabelCodCliente.Location = new System.Drawing.Point(10, 16);
            this.LabelCodCliente.Name = "LabelCodCliente";
            this.LabelCodCliente.Size = new System.Drawing.Size(64, 13);
            this.LabelCodCliente.TabIndex = 0;
            this.LabelCodCliente.Text = "Cód. Cliente";
            // 
            // TextBoxCodCliente
            // 
            this.TextBoxCodCliente.Location = new System.Drawing.Point(80, 13);
            this.TextBoxCodCliente.Name = "TextBoxCodCliente";
            this.TextBoxCodCliente.Size = new System.Drawing.Size(69, 20);
            this.TextBoxCodCliente.TabIndex = 1;
            this.TextBoxCodCliente.Text = "C001";
            // 
            // LabelNome
            // 
            this.LabelNome.AutoSize = true;
            this.LabelNome.Location = new System.Drawing.Point(161, 16);
            this.LabelNome.Name = "LabelNome";
            this.LabelNome.Size = new System.Drawing.Size(35, 13);
            this.LabelNome.TabIndex = 2;
            this.LabelNome.Text = "Nome";
            // 
            // TextBoxNome
            // 
            this.TextBoxNome.Location = new System.Drawing.Point(202, 13);
            this.TextBoxNome.Name = "TextBoxNome";
            this.TextBoxNome.Size = new System.Drawing.Size(308, 20);
            this.TextBoxNome.TabIndex = 3;
            this.TextBoxNome.Text = "Paulo Félix";
            // 
            // GroupBoxResgistoCompras
            // 
            this.GroupBoxResgistoCompras.Controls.Add(this.ButtonRegistarCompra);
            this.GroupBoxResgistoCompras.Controls.Add(this.NumericUpDownValorCompra);
            this.GroupBoxResgistoCompras.Controls.Add(this.LabelValorCompra);
            this.GroupBoxResgistoCompras.Controls.Add(this.TextBoxDescricao);
            this.GroupBoxResgistoCompras.Controls.Add(this.LabelDescricao);
            this.GroupBoxResgistoCompras.Location = new System.Drawing.Point(16, 84);
            this.GroupBoxResgistoCompras.Name = "GroupBoxResgistoCompras";
            this.GroupBoxResgistoCompras.Size = new System.Drawing.Size(516, 72);
            this.GroupBoxResgistoCompras.TabIndex = 4;
            this.GroupBoxResgistoCompras.TabStop = false;
            this.GroupBoxResgistoCompras.Text = "Registar compras";
            // 
            // ButtonRegistarCompra
            // 
            this.ButtonRegistarCompra.Enabled = false;
            this.ButtonRegistarCompra.Location = new System.Drawing.Point(400, 40);
            this.ButtonRegistarCompra.Name = "ButtonRegistarCompra";
            this.ButtonRegistarCompra.Size = new System.Drawing.Size(110, 23);
            this.ButtonRegistarCompra.TabIndex = 4;
            this.ButtonRegistarCompra.Text = "Comprar";
            this.ButtonRegistarCompra.UseVisualStyleBackColor = true;
            this.ButtonRegistarCompra.Click += new System.EventHandler(this.ButtonRegistarCompra_Click);
            // 
            // NumericUpDownValorCompra
            // 
            this.NumericUpDownValorCompra.DecimalPlaces = 2;
            this.NumericUpDownValorCompra.Enabled = false;
            this.NumericUpDownValorCompra.Location = new System.Drawing.Point(104, 44);
            this.NumericUpDownValorCompra.Name = "NumericUpDownValorCompra";
            this.NumericUpDownValorCompra.Size = new System.Drawing.Size(120, 20);
            this.NumericUpDownValorCompra.TabIndex = 3;
            // 
            // LabelValorCompra
            // 
            this.LabelValorCompra.AutoSize = true;
            this.LabelValorCompra.Location = new System.Drawing.Point(11, 48);
            this.LabelValorCompra.Name = "LabelValorCompra";
            this.LabelValorCompra.Size = new System.Drawing.Size(84, 13);
            this.LabelValorCompra.TabIndex = 2;
            this.LabelValorCompra.Text = "Valor de compra";
            // 
            // TextBoxDescricao
            // 
            this.TextBoxDescricao.Enabled = false;
            this.TextBoxDescricao.Location = new System.Drawing.Point(104, 18);
            this.TextBoxDescricao.Name = "TextBoxDescricao";
            this.TextBoxDescricao.Size = new System.Drawing.Size(406, 20);
            this.TextBoxDescricao.TabIndex = 1;
            // 
            // LabelDescricao
            // 
            this.LabelDescricao.AutoSize = true;
            this.LabelDescricao.Location = new System.Drawing.Point(40, 22);
            this.LabelDescricao.Name = "LabelDescricao";
            this.LabelDescricao.Size = new System.Drawing.Size(55, 13);
            this.LabelDescricao.TabIndex = 0;
            this.LabelDescricao.Text = "Descrição";
            // 
            // GroupBoxMudarNome
            // 
            this.GroupBoxMudarNome.Controls.Add(this.ButtonMudarNome);
            this.GroupBoxMudarNome.Controls.Add(this.TextBoxMudarNome);
            this.GroupBoxMudarNome.Controls.Add(this.LabelMudarNome);
            this.GroupBoxMudarNome.Location = new System.Drawing.Point(16, 162);
            this.GroupBoxMudarNome.Name = "GroupBoxMudarNome";
            this.GroupBoxMudarNome.Size = new System.Drawing.Size(268, 106);
            this.GroupBoxMudarNome.TabIndex = 5;
            this.GroupBoxMudarNome.TabStop = false;
            this.GroupBoxMudarNome.Text = "Mudar titular do cartão";
            // 
            // ButtonMudarNome
            // 
            this.ButtonMudarNome.Enabled = false;
            this.ButtonMudarNome.Location = new System.Drawing.Point(176, 75);
            this.ButtonMudarNome.Name = "ButtonMudarNome";
            this.ButtonMudarNome.Size = new System.Drawing.Size(75, 23);
            this.ButtonMudarNome.TabIndex = 2;
            this.ButtonMudarNome.Text = "Alterar";
            this.ButtonMudarNome.UseVisualStyleBackColor = true;
            this.ButtonMudarNome.Click += new System.EventHandler(this.ButtonMudarNome_Click);
            // 
            // TextBoxMudarNome
            // 
            this.TextBoxMudarNome.Enabled = false;
            this.TextBoxMudarNome.Location = new System.Drawing.Point(52, 20);
            this.TextBoxMudarNome.Multiline = true;
            this.TextBoxMudarNome.Name = "TextBoxMudarNome";
            this.TextBoxMudarNome.Size = new System.Drawing.Size(199, 50);
            this.TextBoxMudarNome.TabIndex = 1;
            // 
            // LabelMudarNome
            // 
            this.LabelMudarNome.AutoSize = true;
            this.LabelMudarNome.Location = new System.Drawing.Point(10, 20);
            this.LabelMudarNome.Name = "LabelMudarNome";
            this.LabelMudarNome.Size = new System.Drawing.Size(35, 13);
            this.LabelMudarNome.TabIndex = 0;
            this.LabelMudarNome.Text = "Nome";
            // 
            // GroupBoxRegistarCliente
            // 
            this.GroupBoxRegistarCliente.Controls.Add(this.ButtonRegistarCliente);
            this.GroupBoxRegistarCliente.Controls.Add(this.LabelCodCliente);
            this.GroupBoxRegistarCliente.Controls.Add(this.TextBoxCodCliente);
            this.GroupBoxRegistarCliente.Controls.Add(this.LabelNome);
            this.GroupBoxRegistarCliente.Controls.Add(this.TextBoxNome);
            this.GroupBoxRegistarCliente.Location = new System.Drawing.Point(16, 12);
            this.GroupBoxRegistarCliente.Name = "GroupBoxRegistarCliente";
            this.GroupBoxRegistarCliente.Size = new System.Drawing.Size(516, 66);
            this.GroupBoxRegistarCliente.TabIndex = 6;
            this.GroupBoxRegistarCliente.TabStop = false;
            this.GroupBoxRegistarCliente.Text = "Registar cliente";
            // 
            // ButtonRegistarCliente
            // 
            this.ButtonRegistarCliente.Location = new System.Drawing.Point(400, 38);
            this.ButtonRegistarCliente.Name = "ButtonRegistarCliente";
            this.ButtonRegistarCliente.Size = new System.Drawing.Size(110, 23);
            this.ButtonRegistarCliente.TabIndex = 4;
            this.ButtonRegistarCliente.Text = "Registar";
            this.ButtonRegistarCliente.UseVisualStyleBackColor = true;
            this.ButtonRegistarCliente.Click += new System.EventHandler(this.ButtonRegistarCliente_Click);
            // 
            // GroupBoxMudarTaxaPontos
            // 
            this.GroupBoxMudarTaxaPontos.Controls.Add(this.ButtonMudarTaxaPontos);
            this.GroupBoxMudarTaxaPontos.Controls.Add(this.LabelTaxaAtual);
            this.GroupBoxMudarTaxaPontos.Controls.Add(this.LabelNovaTaxa);
            this.GroupBoxMudarTaxaPontos.Controls.Add(this.NumericUpDownNovaTaxa);
            this.GroupBoxMudarTaxaPontos.Location = new System.Drawing.Point(283, 162);
            this.GroupBoxMudarTaxaPontos.Name = "GroupBoxMudarTaxaPontos";
            this.GroupBoxMudarTaxaPontos.Size = new System.Drawing.Size(249, 106);
            this.GroupBoxMudarTaxaPontos.TabIndex = 7;
            this.GroupBoxMudarTaxaPontos.TabStop = false;
            this.GroupBoxMudarTaxaPontos.Text = "Mudar taxa de pontos";
            // 
            // ButtonMudarTaxaPontos
            // 
            this.ButtonMudarTaxaPontos.Enabled = false;
            this.ButtonMudarTaxaPontos.Location = new System.Drawing.Point(129, 70);
            this.ButtonMudarTaxaPontos.Name = "ButtonMudarTaxaPontos";
            this.ButtonMudarTaxaPontos.Size = new System.Drawing.Size(109, 23);
            this.ButtonMudarTaxaPontos.TabIndex = 3;
            this.ButtonMudarTaxaPontos.Text = "Alterar";
            this.ButtonMudarTaxaPontos.UseVisualStyleBackColor = true;
            this.ButtonMudarTaxaPontos.Click += new System.EventHandler(this.ButtonMudarTaxaPontos_Click);
            // 
            // LabelTaxaAtual
            // 
            this.LabelTaxaAtual.AutoSize = true;
            this.LabelTaxaAtual.Location = new System.Drawing.Point(66, 21);
            this.LabelTaxaAtual.Name = "LabelTaxaAtual";
            this.LabelTaxaAtual.Size = new System.Drawing.Size(80, 13);
            this.LabelTaxaAtual.TabIndex = 2;
            this.LabelTaxaAtual.Text = "Taxa atual: 0 %";
            // 
            // LabelNovaTaxa
            // 
            this.LabelNovaTaxa.AutoSize = true;
            this.LabelNovaTaxa.Location = new System.Drawing.Point(39, 46);
            this.LabelNovaTaxa.Name = "LabelNovaTaxa";
            this.LabelNovaTaxa.Size = new System.Drawing.Size(56, 13);
            this.LabelNovaTaxa.TabIndex = 1;
            this.LabelNovaTaxa.Text = "Nova taxa";
            // 
            // NumericUpDownNovaTaxa
            // 
            this.NumericUpDownNovaTaxa.DecimalPlaces = 2;
            this.NumericUpDownNovaTaxa.Enabled = false;
            this.NumericUpDownNovaTaxa.Location = new System.Drawing.Point(103, 43);
            this.NumericUpDownNovaTaxa.Name = "NumericUpDownNovaTaxa";
            this.NumericUpDownNovaTaxa.Size = new System.Drawing.Size(110, 20);
            this.NumericUpDownNovaTaxa.TabIndex = 0;
            // 
            // GroupBoxDescontarPontos
            // 
            this.GroupBoxDescontarPontos.Controls.Add(this.ButtonDescontarPontos);
            this.GroupBoxDescontarPontos.Controls.Add(this.NumericUpDownPontosDescontar);
            this.GroupBoxDescontarPontos.Controls.Add(this.label1);
            this.GroupBoxDescontarPontos.Controls.Add(this.LabelPontos);
            this.GroupBoxDescontarPontos.Location = new System.Drawing.Point(16, 275);
            this.GroupBoxDescontarPontos.Name = "GroupBoxDescontarPontos";
            this.GroupBoxDescontarPontos.Size = new System.Drawing.Size(268, 95);
            this.GroupBoxDescontarPontos.TabIndex = 8;
            this.GroupBoxDescontarPontos.TabStop = false;
            this.GroupBoxDescontarPontos.Text = "Descontar pontos";
            // 
            // ButtonDescontarPontos
            // 
            this.ButtonDescontarPontos.Enabled = false;
            this.ButtonDescontarPontos.Location = new System.Drawing.Point(136, 24);
            this.ButtonDescontarPontos.Name = "ButtonDescontarPontos";
            this.ButtonDescontarPontos.Size = new System.Drawing.Size(75, 23);
            this.ButtonDescontarPontos.TabIndex = 3;
            this.ButtonDescontarPontos.Text = "Descontar";
            this.ButtonDescontarPontos.UseVisualStyleBackColor = true;
            this.ButtonDescontarPontos.Click += new System.EventHandler(this.ButtonDescontarPontos_Click);
            // 
            // NumericUpDownPontosDescontar
            // 
            this.NumericUpDownPontosDescontar.Enabled = false;
            this.NumericUpDownPontosDescontar.Location = new System.Drawing.Point(153, 58);
            this.NumericUpDownPontosDescontar.Name = "NumericUpDownPontosDescontar";
            this.NumericUpDownPontosDescontar.Size = new System.Drawing.Size(58, 20);
            this.NumericUpDownPontosDescontar.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(52, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Pontos a descontar";
            // 
            // LabelPontos
            // 
            this.LabelPontos.AutoSize = true;
            this.LabelPontos.Location = new System.Drawing.Point(52, 28);
            this.LabelPontos.Name = "LabelPontos";
            this.LabelPontos.Size = new System.Drawing.Size(52, 13);
            this.LabelPontos.TabIndex = 0;
            this.LabelPontos.Text = "Pontos: 0";
            // 
            // GroupBoxTransferirPontos
            // 
            this.GroupBoxTransferirPontos.Controls.Add(this.ButtonTranferir);
            this.GroupBoxTransferirPontos.Controls.Add(this.NumericUpDownPontosTransferir);
            this.GroupBoxTransferirPontos.Controls.Add(this.LabelPontosTransferir);
            this.GroupBoxTransferirPontos.Controls.Add(this.TextBoxCodCienteTrsf);
            this.GroupBoxTransferirPontos.Controls.Add(this.label2);
            this.GroupBoxTransferirPontos.Location = new System.Drawing.Point(283, 275);
            this.GroupBoxTransferirPontos.Name = "GroupBoxTransferirPontos";
            this.GroupBoxTransferirPontos.Size = new System.Drawing.Size(249, 95);
            this.GroupBoxTransferirPontos.TabIndex = 9;
            this.GroupBoxTransferirPontos.TabStop = false;
            this.GroupBoxTransferirPontos.Text = "Transferir pontos de outro cliente";
            // 
            // ButtonTranferir
            // 
            this.ButtonTranferir.Enabled = false;
            this.ButtonTranferir.Location = new System.Drawing.Point(122, 67);
            this.ButtonTranferir.Name = "ButtonTranferir";
            this.ButtonTranferir.Size = new System.Drawing.Size(99, 23);
            this.ButtonTranferir.TabIndex = 4;
            this.ButtonTranferir.Text = "Transferir";
            this.ButtonTranferir.UseVisualStyleBackColor = true;
            this.ButtonTranferir.Click += new System.EventHandler(this.ButtonTranferir_Click);
            // 
            // NumericUpDownPontosTransferir
            // 
            this.NumericUpDownPontosTransferir.Enabled = false;
            this.NumericUpDownPontosTransferir.Location = new System.Drawing.Point(122, 43);
            this.NumericUpDownPontosTransferir.Name = "NumericUpDownPontosTransferir";
            this.NumericUpDownPontosTransferir.Size = new System.Drawing.Size(99, 20);
            this.NumericUpDownPontosTransferir.TabIndex = 3;
            // 
            // LabelPontosTransferir
            // 
            this.LabelPontosTransferir.AutoSize = true;
            this.LabelPontosTransferir.Location = new System.Drawing.Point(23, 47);
            this.LabelPontosTransferir.Name = "LabelPontosTransferir";
            this.LabelPontosTransferir.Size = new System.Drawing.Size(92, 13);
            this.LabelPontosTransferir.TabIndex = 2;
            this.LabelPontosTransferir.Text = "Pontos a transferir";
            // 
            // TextBoxCodCienteTrsf
            // 
            this.TextBoxCodCienteTrsf.Enabled = false;
            this.TextBoxCodCienteTrsf.Location = new System.Drawing.Point(121, 18);
            this.TextBoxCodCienteTrsf.Name = "TextBoxCodCienteTrsf";
            this.TextBoxCodCienteTrsf.Size = new System.Drawing.Size(100, 20);
            this.TextBoxCodCienteTrsf.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Cód. outro Cliente";
            // 
            // LabelHistorico
            // 
            this.LabelHistorico.AutoSize = true;
            this.LabelHistorico.Location = new System.Drawing.Point(29, 378);
            this.LabelHistorico.Name = "LabelHistorico";
            this.LabelHistorico.Size = new System.Drawing.Size(48, 13);
            this.LabelHistorico.TabIndex = 10;
            this.LabelHistorico.Text = "Histórico";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(545, 603);
            this.Controls.Add(this.LabelHistorico);
            this.Controls.Add(this.GroupBoxTransferirPontos);
            this.Controls.Add(this.GroupBoxDescontarPontos);
            this.Controls.Add(this.GroupBoxMudarTaxaPontos);
            this.Controls.Add(this.GroupBoxRegistarCliente);
            this.Controls.Add(this.GroupBoxMudarNome);
            this.Controls.Add(this.GroupBoxResgistoCompras);
            this.Name = "Form1";
            this.Text = "Form1";
            this.GroupBoxResgistoCompras.ResumeLayout(false);
            this.GroupBoxResgistoCompras.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownValorCompra)).EndInit();
            this.GroupBoxMudarNome.ResumeLayout(false);
            this.GroupBoxMudarNome.PerformLayout();
            this.GroupBoxRegistarCliente.ResumeLayout(false);
            this.GroupBoxRegistarCliente.PerformLayout();
            this.GroupBoxMudarTaxaPontos.ResumeLayout(false);
            this.GroupBoxMudarTaxaPontos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownNovaTaxa)).EndInit();
            this.GroupBoxDescontarPontos.ResumeLayout(false);
            this.GroupBoxDescontarPontos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownPontosDescontar)).EndInit();
            this.GroupBoxTransferirPontos.ResumeLayout(false);
            this.GroupBoxTransferirPontos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownPontosTransferir)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LabelCodCliente;
        private System.Windows.Forms.TextBox TextBoxCodCliente;
        private System.Windows.Forms.Label LabelNome;
        private System.Windows.Forms.TextBox TextBoxNome;
        private System.Windows.Forms.GroupBox GroupBoxResgistoCompras;
        private System.Windows.Forms.NumericUpDown NumericUpDownValorCompra;
        private System.Windows.Forms.Label LabelValorCompra;
        private System.Windows.Forms.TextBox TextBoxDescricao;
        private System.Windows.Forms.Label LabelDescricao;
        private System.Windows.Forms.Button ButtonRegistarCompra;
        private System.Windows.Forms.GroupBox GroupBoxMudarNome;
        private System.Windows.Forms.TextBox TextBoxMudarNome;
        private System.Windows.Forms.Label LabelMudarNome;
        private System.Windows.Forms.Button ButtonMudarNome;
        private System.Windows.Forms.GroupBox GroupBoxRegistarCliente;
        private System.Windows.Forms.Button ButtonRegistarCliente;
        private System.Windows.Forms.GroupBox GroupBoxMudarTaxaPontos;
        private System.Windows.Forms.Button ButtonMudarTaxaPontos;
        private System.Windows.Forms.Label LabelTaxaAtual;
        private System.Windows.Forms.Label LabelNovaTaxa;
        private System.Windows.Forms.NumericUpDown NumericUpDownNovaTaxa;
        private System.Windows.Forms.GroupBox GroupBoxDescontarPontos;
        private System.Windows.Forms.Label LabelPontos;
        private System.Windows.Forms.NumericUpDown NumericUpDownPontosDescontar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button ButtonDescontarPontos;
        private System.Windows.Forms.GroupBox GroupBoxTransferirPontos;
        private System.Windows.Forms.NumericUpDown NumericUpDownPontosTransferir;
        private System.Windows.Forms.Label LabelPontosTransferir;
        private System.Windows.Forms.TextBox TextBoxCodCienteTrsf;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button ButtonTranferir;
        private System.Windows.Forms.Label LabelHistorico;
    }
}

