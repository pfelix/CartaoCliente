﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CartaoCliente
{
    public partial class Form1 : Form
    {

        private readonly CartaoCliente myCartaoCliente;

        public Form1()
        {
            InitializeComponent();
            myCartaoCliente = new CartaoCliente();
        }

        private void ButtonRegistarCliente_Click(object sender, EventArgs e)
        {
            if (!(TextBoxCodCliente.Text == "" || TextBoxNome.Text == ""))
            {
                myCartaoCliente.CodCliente = TextBoxCodCliente.Text;
                myCartaoCliente.Nome = TextBoxNome.Text;

                TextBoxCodCliente.Enabled = false;
                TextBoxNome.Enabled = false;
                ButtonRegistarCliente.Enabled = false;

                TextBoxDescricao.Enabled = true;
                NumericUpDownValorCompra.Enabled = true;
                ButtonRegistarCompra.Enabled = true;

                TextBoxMudarNome.Enabled = true;
                ButtonMudarNome.Enabled = true;

                NumericUpDownNovaTaxa.Enabled = true;
                ButtonMudarTaxaPontos.Enabled = true;
                LabelTaxaAtual.Text = "Taxa atual: " + myCartaoCliente.TaxaPontos * 100 + " %";

                LabelPontos.Text = "Pontos: " + myCartaoCliente.Pontos.ToString();

                ButtonDescontarPontos.Enabled = true;
                NumericUpDownPontosDescontar.Enabled = true;

                TextBoxCodCienteTrsf.Enabled = true;
                NumericUpDownPontosTransferir.Enabled = true;
                ButtonTranferir.Enabled = true;
            }
        }

        private void ButtonRegistarCompra_Click(object sender, EventArgs e)
        {
            if (!(TextBoxDescricao.Text == ""))
            {
                myCartaoCliente.Compra(TextBoxDescricao.Text, Convert.ToDouble(NumericUpDownValorCompra.Value));
                LabelPontos.Text = "Pontos: " + myCartaoCliente.Pontos.ToString();
                LabelHistorico.Text = myCartaoCliente.ToString();
            }
        }

        private void ButtonMudarNome_Click(object sender, EventArgs e)
        {
            if (!(TextBoxMudarNome.Text == ""))
            {
                myCartaoCliente.Nome = TextBoxMudarNome.Text;
                TextBoxNome.Text = myCartaoCliente.Nome;
            }
        }

        private void ButtonMudarTaxaPontos_Click(object sender, EventArgs e)
        {
            myCartaoCliente.TaxaPontos = Convert.ToDouble(NumericUpDownNovaTaxa.Value/100);
            LabelTaxaAtual.Text = "Taxa atual: " + myCartaoCliente.TaxaPontos * 100 + " %";
        }

        private void ButtonDescontarPontos_Click(object sender, EventArgs e)
        {
            myCartaoCliente.DescontaPontosCartao(Convert.ToInt32(NumericUpDownPontosDescontar.Value));
            LabelPontos.Text = "Pontos: " + myCartaoCliente.Pontos.ToString();
        }

        private void ButtonTranferir_Click(object sender, EventArgs e)
        {
            if (!(TextBoxCodCienteTrsf.Text == ""))
            {
                myCartaoCliente.TransferirPontos(Convert.ToInt32(NumericUpDownPontosTransferir.Value));
                LabelPontos.Text = "Pontos: " + myCartaoCliente.Pontos.ToString();
            }
        }
    }
}
