﻿namespace CartaoCliente
{
    using System;
    using System.Text;

    public class CartaoCliente
    {
        #region Atributos

        private string[,] _compra = new string[20, 2];
        private readonly double _totalAcumulado;
        private readonly int _pontos;
        private double _taxaPontos;
        private string _codCliente;
        private string _nome;

        #endregion

        #region Propriedades

        public double TotalAcumulado { get; private set; }
        public int Pontos { get; private set; }
        public double TaxaPontos { get; set; } //Alterar taxa de prémio
        public string CodCliente { get; set; }
        public string Nome { get; set; } //Alterar titular do cartão

        #endregion

        #region Construtores

        //Defaut
        //public CartaoCliente ()
        //{
        //    Descricao = "Batadas";
        //    TotalCompras = 10;
        //    TotalAcumulado = 10;
        //    Pontos = 1;
        //    TaxaPontos = 10;
        //    CodCliente = "C001";
        //    Nome = "Paulo Félix";
        //}

        public CartaoCliente() : this(0, 0, 0.10, "", "") { }
        //Parametros
        public CartaoCliente(double totalAcumulado, int pontos, double taxaPontos, string codCliente, string nome)
        {
            TotalAcumulado = totalAcumulado;
            Pontos = pontos;
            TaxaPontos = taxaPontos;
            CodCliente = codCliente;
            Nome = nome;
        }
        // Cópia
        public CartaoCliente(CartaoCliente cartaoCliente) : this(cartaoCliente.TotalAcumulado, cartaoCliente.Pontos, cartaoCliente.TaxaPontos, cartaoCliente.CodCliente, cartaoCliente.Nome) { }

        #endregion

        #region Metodos

        int j = 1; // Para o multiplo de 50
        private int AtribuirPontosCartao(double valorCompras) //Atribuir pontos a um cartão
        {
            Pontos = Pontos + Convert.ToInt32(Math.Floor(valorCompras * TaxaPontos)); // Falta verificar o arredondamento para baixo
            
            if (Pontos >= j * 50)
            {
                j++;
                Pontos += 5;
            }

            return Pontos;
        }

        public int DescontaPontosCartao(int pontos) //Desconta pontos do cartão
        {
            int verificaPontos;

            verificaPontos = Pontos - pontos;

            if (!(verificaPontos < 0))
            {
                Pontos = verificaPontos;
            }

            return Pontos;
        }
        int i = 0; // Para alteração do indice de _compra
        public void Compra(string descricao, double valorCompra)//Efetua compra de uma dado valor e atualiza os pontos descrição e o total de compras
        {
            _compra[i, 0] = descricao;
            _compra[i, 1] = Convert.ToString(valorCompra);
            TotalAcumulado += valorCompra;
            AtribuirPontosCartao(valorCompra);
            i++;
        }
        public int TransferirPontos (int pontos)//Passa n pontos de um cartão para o recetor
        {
            Pontos = Pontos + pontos;

            return Pontos;
        }
        public override string ToString()//Modo textual
        {
            StringBuilder txt = new StringBuilder();

            txt.Append("Código cliente: " + CodCliente + "\n");
            txt.Append("Nome: " + Nome + "\n");
            for (int i = 0; i < _compra.GetLength(0); i++)
            {
                if (!(_compra[i, 0] == null))
                {
                    txt.Append("Descrição: " + _compra[i, 0] + "\n");
                    txt.Append("Total de compra: " + _compra[i, 1] + "\n");
                }
            }
            txt.Append("Total acumulado: " + TotalAcumulado + "\n");
            txt.Append("Pontos: " + Pontos + "\n");
            txt.Append("Bonus pontos: " + TaxaPontos + "\n");

            return txt.ToString();

            #endregion


        }
    }
}