﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CartaoClienteTransferencia
{
    using CartaoCliente;

    class Program
    {
        static void Main(string[] args)
        {
            var cartao = new CartaoCliente();
            string codClienteSaida;
            string codClienteEntrada;
            int pontos;

            Console.WriteLine("Indique o códido do cliente de onde vais sair os pontos: ");
            codClienteSaida = Console.ReadLine();
            while (codClienteSaida == "")
            {
                Console.WriteLine("Indique o códido do cliente: ");
                codClienteSaida = Console.ReadLine();
            }

            Console.WriteLine("Indique o códido do cliente de para onde vão os pontos: ");
            codClienteEntrada = Console.ReadLine();
            while (codClienteEntrada == "")
            {
                Console.WriteLine("Indique o códido do cliente de para onde vão os pontos: ");
                codClienteEntrada = Console.ReadLine();
            }

            Console.WriteLine("Indique os pontos: ");
            while(!int.TryParse(Console.ReadLine(), out pontos))
                Console.WriteLine("Indique os pontos: ");

            cartao.TransferirPontos(pontos);

            Console.WriteLine("Foi transferido para o cliente {0}, {1} pontos.", codClienteEntrada, pontos);

            Console.ReadKey();
            
            
        }
    }
}
